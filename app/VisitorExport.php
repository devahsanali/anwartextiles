<?php 
namespace App;

use App\Visitors;
use App\VisitorLog;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class VisitorExport implements FromView
{   
	public $data;
	 public function __construct($data)
    {
        $this->data = $data;
    }
    public function view(): View
    {
        return view('admin.visitor.excel_report', [
            'data' => VisitorLog::with('visitor')->get()
        ]);
    }
}