<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NotificationEmail;
use Redirect;
class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function add(){
        $data = NotificationEmail::get();
        return View('admin.setting.email')->with(['title' => 'Notification Emails', 'data' => $data]);      
    }  
    public function post(Request $request){
        /*$request->validate([
            'email' => 'required|array',
        ]);*/
        if(!isset($request->email) && empty($request->email)){
           return ["error" => "Email field is required."];

        }
        $email = $request->email;
        NotificationEmail::whereNotNull('id')->delete();
        foreach($email as $em){
            $data = new NotificationEmail();
            $data->email = $em;
            $data->save();
        }
      return ["success" => "Successfully Added.", "reload" => true];

    }  
   

}

