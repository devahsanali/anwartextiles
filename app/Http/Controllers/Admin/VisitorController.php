<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Visitor;
use App\VisitorLog;
use App\VisitorExport;

use App\Http\Controllers\Controller;
use Validator;
use Carbon\Carbon;
use PDF;
use Excel;
class VisitorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $data = VisitorLog::with('visitor')->whereDate('check_in', Carbon::today())->get();

        return View('admin.visitor.logs')->with(['title' => 'Visitor Logs', 'data' => $data]);      
    }  
    public function search(Request $request){
        if(isset($request->from_date) && $request->from_date !=''){
            $data = VisitorLog::with('visitor');
            if(isset($request->to_date) && $request->to_date !='' && isset($request->type) && $request->type !=''){
                $data = $data->whereBetween('check_in', [$request->from_date, date("Y-m-d",strtotime("+1 day", strtotime($request->to_date)))]);
            }else{
                $data = $data->whereDate('check_in', $request->from_date); 
            }
            if($request->type != 2){
                $data = $data->where('type', $request->type);
            }
            $data = $data->orderBy('id', 'desc')->get();
            return View('admin.visitor.logs')->with(['title' => 'Search Visitor Logs', 'data' => $data, 'from_date' => $request->from_date, 'to_date' => $request->to_date, 'type' => $request->type,]);      
        }else{
            return redirect(route('home'));
        }
    }  
    public function visitors(Request $request){
        if(isset($request->id) && !empty($request->id)){
            $data = Visitor::where('id', $request->id)->get();
        }else{
            $data = Visitor::get();
        }
        return View('admin.visitor.view')->with(['title' => 'Visitors', 'data' => $data]);      
    }
    public function missing_chekout(){
        $date = Carbon::today();
        $data = Visitor::with(['visitor_logs' => function($q) use($date){
                        $q->whereDate('check_in', $date)->whereNull('check_out'); 
                    }])->get();
        return View('admin.visitor.view')->with(['title' => 'Currenrt Visitors', 'data' => $data, 'missing_chekout' => true]);      
    }   
    public function delete(){
        $data = Visitor::where('id', '>', 0)->delete();
        $data = VisitorLog::where('id', '>', 0)->delete();
        return redirect(route('home'));  
    }

    public function pdf_report(Request $request){
         if(isset($request->from_date) && $request->from_date !=''){
            $data = $this->report_data($request);
            $to_dt = ($request->to_date != '')?date("d-M-Y", strtotime($request->to_date)) : $request->to_date;
            $pdf = PDF::loadview('admin.visitor.pdf_report',['data' => $data, 'from_date' => date("d-M-Y", strtotime($request->from_date)), 'to_date' => $to_dt , 'type' => $request->type,])->setPaper('a4', 'landscape');

            return $pdf->stream('pdf_report.pdf');
         }else{
            return redirect(route('home'));
        }

    }

    public function excel_report(Request $request){
        if(isset($request->from_date) && $request->from_date !=''){
            $data = $this->report_data($request);
            return Excel::download(new VisitorExport($data), 'excel_report.xlsx');
        }else{
            return redirect(route('home'));
        }
       
    }

    public function report_data($request){
        $data = VisitorLog::with('visitor');
        if(isset($request->to_date) && $request->to_date !='' && isset($request->type) && $request->type !=''){
            $data = $data->whereBetween('check_in', [$request->from_date, date("Y-m-d",strtotime("+1 day", strtotime($request->to_date)))]);
        }else{
            $data = $data->whereDate('check_in', $request->from_date); 
        }
        if($request->type != 2){
            $data = $data->where('type', $request->type);
        }
        $data = $data->orderBy('id', 'desc')->get();

        return $data;      
       
    }



}

