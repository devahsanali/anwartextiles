<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class VisitorLog extends Model {

    public function visitor()
    {
        return $this->belongsTo('App\Visitor', 'visitor_id', 'id');
    }
   
}

