<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Visitor extends Model {

    public function visitor_logs(){
        return $this->hasMany('App\VisitorLog', 'visitor_id', 'id');
    }

   
}

