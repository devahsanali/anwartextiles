<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Route::get('/login', function () {
    return view('auth/login');
});


//Auth::routes();
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
	Route::get('/', 'Admin\VisitorController@index')->name('home');
	Route::get('/search', 'Admin\VisitorController@search')->name('visitor.search');
	Route::get('/visitors/{id?}', 'Admin\VisitorController@visitors')->name('visitor.list');
	Route::get('/missing_chekout', 'Admin\VisitorController@missing_chekout')->name('visitor.missing.list');
	Route::get('/visitor/pdf_report', 'Admin\VisitorController@pdf_report')->name('visitor.pdf.report');
	Route::get('/visitor/excel_report', 'Admin\VisitorController@excel_report')->name('visitor.excel.report');
	Route::get('/visitor/delete', 'Admin\VisitorController@delete')->name('delete.data');

	Route::get('logout', 'Auth\LoginController@logout')->name('logout');
	Route::post('/login', 'Auth\LoginController@login')->name('login');
	Route::get('/change-password','Auth\ChangePasswordController@showChangePasswordForm')->name('change.password.form');
	Route::post('/change-password','Auth\ChangePasswordController@changePassword')->name('change.password');
	Route::get('setting/email','Admin\SettingController@add')->name('setting.email');
	Route::post('setting/email','Admin\SettingController@post')->name('setting.email.post');

});
	Route::post('/login', 'Auth\LoginController@login')->name('login');
	Route::get('logout', 'Auth\LoginController@logout')->name('logout');


//Route::get('/home', 'HomeController@index')->name('home');
