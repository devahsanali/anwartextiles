@extends('layouts.admin')

@section('content')
  <div class="page-content">
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All <?= (isset($title))?$title:''; ?></a> </li>
      </ul>
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3>View All - <span class="semi-bold"><?= (isset($title))?$title:''; ?></span></h3>
      </div>
     
      <div class="row-fluid">
        <div class="span12">
          <div class="grid simple">
                    <div class="grid-title no-border">
                        <h4><?= (isset($title))?$title:''; ?> <span class="semi-bold">Form</span></h4>
                    </div>
                    <div class="grid-body no-border">
                        <form class="ajaxForm validate" action="{{route('setting.email.post')}}" method="POST" >
                              @csrf
                                <div class="form-group">
                                        <div class="row" >
                                            <div class="col-md-12">
                                              <div class="" style="width: 100%;">
                                                <table class="table table-bordered">
                                                  <thead>
                                                    <tr>
                                                      <th class="text-center" width="60px">Add/Remove Row</th>
                                                      <th class="text-center" width="200px">Email</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody id="customFields">
                                                    <tr class="txtMult">
                                                      <td class="text-center"><a href="javascript:void(0);" class="addCF">Add</a></td>
                                                      <td colspan="6"></td>
                                                    </tr>
                                                    
                                                     @php($k = 1)
                                                     @foreach($data as $v)
                                                      <tr class="txtMult">
                                                          <td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>
                                                          <td>
                                                              <input type="text" class="email" required style="width:100%" id="email_old{{ $k++ }}"  name="email[]" 
                                                              value="{{$v->email}}" placeholder="Email" />
                                                          </td>
                                                      </tr>
                                                      @endforeach
                                                  </tbody>
                                                </table>
                                              </div>  
                                            </div>
                                    </div>
                                </div>
                            
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                        <button class="btn btn-success btn-cons ajaxFormSubmitAlter" type="submit">
                                           Submit
                                        </button>
                                        <input name="id" type="hidden" value="{{@$id}}">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
      </div>
       
    </div>
    
  </div>
@endsection
@section('footer')
<script>
  $(document).ready(function() {
      jQuery.validator.addMethod("emailValidation", function( value, element ) {
          var regex = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
          var key = value;

          if (!regex.test(key)) {
             return false;
          }
          return true;
      }, "Please enter a valid email address.");

      $("form.validate").validate({
          rules: {
              "email[]": {
                  required: true,
                  emailValidation : true,
              }
          },
          messages: {

          },
          invalidHandler: function(event, validator) {
              error("Please input all the mandatory values marked as red");

          },
          errorPlacement: function(label, element) { // render error placement for each input type   
              var icon = $(element).parent('.input-with-icon').children('i');
              icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
              $('<span class="error"></span>').insertAfter(element).append(label);
              var parent = $(element).parent('.input-with-icon');
              parent.removeClass('success-control').addClass('error-control');
          },
          highlight: function(element) { // hightlight error inputs
              var icon = $(element).parent('.input-with-icon').children('i');
              icon.removeClass('fa fa-check').addClass('fa fa-exclamation');
              var parent = $(element).parent();
              parent.removeClass('success-control').addClass('error-control');
          },
          unhighlight: function(element) { // revert the change done by hightlight
              var icon = $(element).parent('.input-with-icon').children('i');
              icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
              var parent = $(element).parent();
              parent.removeClass('error-control').addClass('success-control');
          },
          success: function(label, element) {
                  var icon = $(element).parent('.input-with-icon').children('i');
                  icon.removeClass("fa fa-exclamation").addClass('fa fa-check');
                  var parent = $(element).parent('.input-with-icon');
                  parent.removeClass('error-control').addClass('success-control');

              }
              // submitHandler: function (form) {
              // }
      });
      var max_fields      = 6; 
      var add_button      = $("#customFields .addCF");
      var x = 1; 
      $(add_button).click(function(e){
        e.preventDefault();
        $('form.validate').validate();
          x++;
          var temp = '<tr class="txtMult">';
          temp    += '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remCF">Remove</a></td>';
          temp    += "<td>";
          temp    += '<input type="text" class="email" required  style="width:100%" id="email'+x+'" data-optional="0" name="email[]" value="" placeholder="" />';
          temp    += '</td>';
          temp    += '</tr>';
          $("#customFields").append(temp);
      });
      $("#customFields").on('click','.remCF',function(){
          $(this).parent().parent().remove();
      });

  });
</script>
@endsection