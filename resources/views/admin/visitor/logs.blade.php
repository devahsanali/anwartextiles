@extends('layouts.admin')

@section('content')
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All <?= (isset($title))?$title:''; ?></a> </li>
      </ul>
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3>View All - <span class="semi-bold"><?= (isset($title))?$title:''; ?></span></h3>
      </div>
     
          <div class="row-fluid">
            <div class="span12">
              <div class="grid simple ">
                  
                   <div class="pb-30 card-view search-filtr">
                    <div class="pull-left mt-10 mb-10">
                      <h5>Search Filter</h5>
                    </div>
                    <div class="clearfix"></div>
                    <form class="custom-search" action="{{ route('visitor.search') }}" method="GET">
                        <select name="type" id="search_type" class="search_type select2 search-type-width" >
                          <option {{(!isset($type) || (isset($type) && $type == 2))? 'selected' : ''}} value="2">Both</option>
                          <option {{(isset($type) && $type == 1)? 'selected' : ''}} value="1">Corporate</option>
                          <option {{(isset($type) && $type == 0)? 'selected' : ''}} value="0">Production</option>
                        </select>     
                        <!-- <label>Date:</label>   -->
                        <input type="text" class="datepicker from_date" placeholder="From" name='from_date' value='<?= @$from_date?>' required>
                        <input type="text" class="datepicker to_date" name='to_date' placeholder="To" value='<?= @$to_date?>' >
                          
                        <button type="submit" class="btn btn-outline-info custm-srch-btn">
                        Search
                        </button>
                        <button type="button" data="{{route('visitor.pdf.report')}}" class="report_btn btn btn-outline-info custm-srch-btn">
                        PDF Report
                        </button>
                         <button type="button" data="{{route('visitor.excel.report')}}" class="report_btn btn btn-outline-info custm-srch-btn">
                        Excel Report
                        </button>
                          
                      </form>
                </div>
                <div class="clearfix"></div>
                <br>
                
                <div class="grid-title">
                  <h4>View All <span class="semi-bold"><?= (isset($title))?$title:''; ?></span></h4>
                  <div class="pull-right">
                  </div>
                </div>
                <div class="grid-body ">
                  <table class="table" id="example3" >
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Type</th>
                        <th>Visitor Number (QR-Code)</th>
                        <th>Visitor Name</th>
                        <th>Visitor Contact</th>
                        <th>Visitor CNIC</th>
                        <th>Visitor Other ID</th>
                        <th>Visitor Company</th>
                        <th>Visitor Temperature</th>
                        <th>Contact Person</th>
                        <th>Purpose</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                      </tr>
                    </thead>
                    <tbody>
                    @php ($count = 1)
                      @foreach( $data as $v)
                      <tr class="">
                        <td>{{ $count++ }}</td>
                        <td>{{ ($v->type == 1)? 'Corporate' : 'Production' }}</td>
                        <td>{{ $v->qr_code }}</td>
                        <td><a target="_blank" href="{{route('visitor.list', $v->visitor['id'])}}">{{ $v->visitor['name'] }}</a></td>
                        <td>{{ $v->visitor['phone'] }}</td>
                        <td>{{ $v->visitor['nic'] }}</td>
                        <td>{{ $v->visitor['other_card'] }}</td>
                        <td>{{ $v->visitor['company'] }}</td>
                        <td>{{ $v->temperature }}F</td>
                        <td>{{ $v->contact_person }}</td>
                        <td>{{ $v->purpose }}</td>
                        <td>{{ date('d-M-Y H:i:s A', strtotime($v->check_in)) }}</td>
                        <td><?= ($v->check_out == '')? 'N/A' :  date('d-M-Y H:i:s A', strtotime($v->check_out)) ?></td>
                       </tr>
                       @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
       
    </div>
    
  </div>
@endsection
@section('footer')
  <script>     
    $(document).ready(function(){
        $( ".datepicker" ).datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        });
        //$(".from_date").datepicker().datepicker("setDate", new Date());
        $( ".report_btn" ).click(function() {
          var route = $(this).attr('data');
          var from = $(".from_date").val();
          var to = $(".to_date").val();
          var type = $('#search_type').find(':selected').val();
          var url = route+"?type="+type+"&from_date="+from+"&to_date="+to;
          //window.location.href = url;
          window.open(url, "_blank");

         // console.log($(this).attr('data'));
        })
        
    });  

  </script>
@endsection