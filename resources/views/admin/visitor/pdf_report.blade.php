<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<style>
    body  { font-family: DejaVu Sans, sans-serif; }
    @page { margin:10px 2% 10px 0; }
    body  { margin: 10px;  }
      
      .table1{
        font-size: 12px;
      }
      .table2{
        font-size: 10px;
      }
      .table_align{
            margin-left: 10%;
            margin-right: 10%;
      }
      .table1-td-h4 {
            color: #333333;
      }
      .table1-td-h4-2{
            color: #7f7f7f;
      }
      .table1-td-quotHeading {
            text-align: right;
            font-size: 24px;
            /*font-weight: bold;*/
      }
      .product-table1-txt-align {
            position: relative;
            right: 0;
      }
      .product-table2-th-details {
            /*border: 1px solid #adadad; */
            background-color:#3c3d3a;
            color: #fff;
      }
      .product-table2-td-details {
            border:1px solid #adadad;
      }
      .div-controls{
        margin-left: 8.5%;
        margin-right: 8.5%;
      }
      .div-font-controls{
        font-family: Calibri;
        font-size: 12px;
      }
      /*@page toc { sheet-size: A4; }
      span, strong, em, i, p{
        font-family: calibri;
      }*/
      .td-border{
        border: 7px double #000;
      }
</style>
</head>
<body>
          <br/>
          <table width="100%" class="table1">
            <tr>
              <td width="130px" >&nbsp;</td>
              <td width="140px"  align="right">
                <img src="{{ public_path("/assets/admin/img/logo.png") }}" class="logo" alt="" width="120px" height="60px">
              </td>
              <td width="420px" align="center">
                <h1>Anwartex Industries (Pvt) Ltd 
                <br>
                Visitor's Book</h1>
              </td>
              <td width="260px"  align="right">
                From Date :&nbsp;<span style="border-bottom: 1px solid #000;">&nbsp;&nbsp;{{$from_date}} &nbsp;&nbsp;</span>
                <br/>
                To Date &nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;<span style="border-bottom: 1px solid #000;">&nbsp;&nbsp; {{$to_date}} &nbsp;&nbsp;</span>
              </td>
            </tr>
          </table>
          <br/>
          <table style="border-collapse: collapse;" width="100%" class="table2">
              <thead>

                  <tr>
                    
                    <th class="product-table2-th-details" height="30px" style="vertical-align:bottom;padding:50px 10px 3px 10px;" width="30px">S#</th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--زائرین نمبر                    -->
                    <!--<br/><br/><br/><br/>  -->
                    VISITOR #
                    </th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--زائرین کی قسم-->
                    <!--<br/><br/>  -->
                    VISITOR TYPE
                    </th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--زائرین کا نام-->
                    <!--<br/><br/><br/>  -->
                    VISITOR NAME
                    </th>

                    <th class="product-table2-th-details" height="30px" width="60px" style="padding:50px 10px 3px 10px;vertical-align:bottom;">
                      <!--قومی شناختی کارڈ نمبر-->
                      <!--<br/><br/><br/> -->
                      N.I.C #
                    </th>


                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--کمپنی کا نام-->
                    <!--<br/><br/><br/>-->
                    COMPANY NAME
                    </th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--رابطے کا بندہ-->
                    <!--<br/><br/><br/>-->
                    CONTACT PERSON
                    </th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--مقصد-->
                    <!--<br/><br/><br/><br/>-->
                    PURPOSE
                    </th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--درجہ حرارت-->
                    <!--<br/><br/><br/><br/>-->
                    TEMPERATURE
                    </th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--دوسری شناخت-->
                    <!--<br/><br/><br/>-->
                    OTHER ID
                    </th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--آنے کا وقت-->
                    <!--<br/><br/><br/><br/>-->
                    TIME IN
                    </th>

                    <th class="product-table2-th-details" style="vertical-align:bottom;padding:50px 10px 3px 10px;" height="30px" width="60px">
                    <!--جانے کا وقت-->
                    <!--<br/><br/><br/><br/>-->
                    TIME OUT
                    </th>
                  
                  </tr>

              </thead>
              <tbody>
                @php ($count = 1)
                @foreach( $data as $v)  
                  <tr>
                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $count++ }}</td>
                      
                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $v->qr_code }}</td>
                    
                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ ($v->type == 1)? 'Corporate' : 'Production' }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $v->visitor['name'] }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $v->visitor['nic'] }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $v->visitor['company'] }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $v->contact_person }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $v->purpose }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $v->temperature }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ $v->visitor['other_card'] }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      {{ date('d-M-Y H:i:s A', strtotime($v->check_in)) }}</td>

                      <td align="center" class="product-table2-td-details" height="30px" style="padding:10px 5px;">
                      <?= ($v->check_out == '')? 'N/A' :  date('d-M-Y H:i:s A', strtotime($v->check_out)) ?></td>
                  </tr>
                @endforeach
              </tbody>

            </table>
</body>
</html>
 