@extends('layouts.admin')

@section('content')
  <div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div class="content">
      <ul class="breadcrumb">
        <li>
          <p>Dashboard</p>
        </li>
        <li><a href="#" class="active">View All <?= (isset($title))?$title:''; ?></a> </li>
      </ul>
      <div class="page-title"> <i class="icon-custom-left"></i>
        <h3>View All - <span class="semi-bold"><?= (isset($title))?$title:''; ?></span></h3>
      </div>
     
          <div class="row-fluid">
            <div class="span12">
              <div class="grid simple ">
                  
                  <!--  <div class="pb-30 card-view search-filtr">
                   <div class="pull-left mt-10 mb-10">
                     <h5>Search Filter</h5>
                   </div>
                   <div class="clearfix"></div>
                   <form class="custom-search" action="{{ route('visitor.search') }}" method="GET">
                             
                       <label>Date:</label>  
                       <input type="text" class="datepicker from_date" placeholder="From" name='from_date' value='<?= @$from_date?>' required>
                       <input type="text" class="datepicker" name='to_date' placeholder="To" value='<?= @$to_date?>' >
                         
                       <button type="submit" class="btn btn-outline-info custm-srch-btn">
                       Search
                       </button>
                         
                     </form>
                                  </div> -->
                <div class="clearfix"></div>
                <br>
                
                <div class="grid-title">
                  <h4>View All <span class="semi-bold"><?= (isset($title))?$title:''; ?></span></h4>
                  <div class="pull-right">
                  </div>
                </div>
                <div class="grid-body ">
                  <table class="table" id="example3" >
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Photo</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>N.I.C.</th>
                        <th>Company</th>
                        <th>Registered at</th>
                        
                      </tr>
                    </thead>
                    <tbody>
                    @php ($count = 1)
                      @foreach( $data as $v)
                      <tr class="">
                        <td>{{ $count++ }}</td>
                        <td><a href="#" class="pop"><img width="50" src="{{ env('VISITOR_IMAGES') }}{{ $v->photo }}"/></a></td>
                        <td>{{ $v->name }}</td>
                        <td>{{ $v->phone }}</td>
                        <td>{{ $v->nic }}</td>
                        <td>{{ $v->company }}</td>
                        <td>{{ date('d-M-Y H:i:s A', strtotime($v->created_at)) }}</td>
                       </tr>
                       @endforeach
                    </tbody>
                  </table>
                </div>
                    <!-- The Modal -->
                    <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog" data-dismiss="modal">
                        <div class="modal-content"  >              
                          <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <img src="" class="imagepreview" style="width: 100%;" >
                          </div> 
                          <div class="modal-footer">
                          </div>
                              
                              
                        </div>
                      </div>
                    </div>
              </div>
            </div>
          </div>
       
    </div>
    
  </div>
@endsection
@section('footer')
<!--   <script>     
  $(document).ready(function(){
      $( ".datepicker" ).datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
      });
      //$(".from_date").datepicker().datepicker("setDate", new Date());

      
  });    
</script> -->


<script>
$(function() {
    $('.pop').on('click', function() {
      $('.imagepreview').attr('src', $(this).find('img').attr('src'));
      $('#imagemodal').modal('show');   
    });   
});
</script>
@endsection