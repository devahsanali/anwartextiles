 <table class="table" id="example3" >
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th>Type</th>
                        <th>Visitor Number (QR-Code)</th>
                        <th>Visitor Name</th>
                        <th>Visitor Contact</th>
                        <th>Visitor CNIC</th>
                        <th>Visitor Other ID</th>
                        <th>Visitor Company</th>
                        <th>Visitor Temperature</th>
                        <th>Contact Person</th>
                        <th>Purpose</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                      </tr>
                    </thead>
                    <tbody>
                    @php ($count = 1)
                      @foreach( $data as $v)
                      <tr class="">
                        <td>{{ $count++ }}</td>
                        <td>{{ ($v->type == 1)? 'Corporate' : 'Production' }}</td>
                        <td>{{ $v->qr_code }}</td>
                        <td>{{ $v->visitor['name'] }}</td>
                        <td>{{ $v->visitor['phone'] }}</td>
                        <td>{{ $v->visitor['nic'] }}</td>
                        <td>{{ $v->visitor['other_card'] }}</td>
                        <td>{{ $v->visitor['company'] }}</td>
                        <td>{{ $v->temperature }}</td>
                        <td>{{ $v->contact_person }}</td>
                        <td>{{ $v->purpose }}</td>
                        <td>{{ date('d-M-Y H:i:s A', strtotime($v->check_in)) }}</td>
                        <td><?= ($v->check_out == '')? 'N/A' :  date('d-M-Y H:i:s A', strtotime($v->check_out)) ?></td>
                       </tr>
                       @endforeach
                    </tbody>
                  </table>