<!--BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu">
    <!-- BEGIN MINI-PROFILE -->
    <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
      <ul>

        <li class="{{ ( Route::currentRouteName() == 'home') ? 'active open' : '' }}>">
           <a href="{{ route('home') }}"> <i class="fa fa-calculator" aria-hidden="true"></i>
            <span class="title">Visitor Logs</span>
          </a> 
        </li>
         <li class="{{ ( Route::currentRouteName() == 'visitor.list') ? 'active open' : '' }}>">
           <a href="{{ route('visitor.list') }}"> <i class="fa fa-users" aria-hidden="true"></i>
            <span class="title">Visitors</span>
          </a> 
        </li>
         <li class="{{ ( Route::currentRouteName() == 'change.password.form') ? 'active open' : '' }}>">
           <a href="{{ route('change.password.form') }}"> <i class="fa fa-cogs" aria-hidden="true"></i>
            <span class="title">Change Password</span>
          </a> 
        </li>
        <li class="{{ ( Route::currentRouteName() == 'setting.email') ? 'active open' : '' }}>">
           <a href="{{ route('setting.email') }}"> <i class="fa fa-envelope" aria-hidden="true"></i>
            <span class="title">Notification Email</span>
          </a> 
        </li>
        <li class="{{ ( Route::currentRouteName() == 'delete.data') ? 'active open' : '' }}>">
           <a href="{{ route('delete.data') }}"> <i class="fa fa-trash" aria-hidden="true"></i>
            <span class="title">Delete</span>
          </a> 
        </li>
        <li class="{{ ( Route::currentRouteName() == 'logout') ? 'active open' : '' }}>">
           <a href="{{ route('logout') }}"> <i class="fa fa-power-off" aria-hidden="true"></i>
            <span class="title">Logout</span>
          </a> 
        </li>


      </ul>
      <div class="clearfix"></div>
      <!-- END SIDEBAR MENU -->
    </div>
  </div>
  <a href="#" class="scrollup">Scroll</a>